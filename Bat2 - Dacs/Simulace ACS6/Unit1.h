//---------------------------------------------------------------------------
#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
    TButton *Button1;
    TLabel *Label1;
    TLabel *Label2;
        TButton *RandomButton;
        TLabel *Label3;
        TComboBox *ComboBoxPort;
        TGroupBox *GroupBox1;
        TLabel *Label4;
        TLabel *Label5;
        TEdit *EditProductionDay;
        TDateTimePicker *DateTimePickerDate;
        TButton *ButtonSend;
        TLabel *Label6;
        TDateTimePicker *DateTimePickerTime;
        TLabel *Label7;
        TListBox *ListBoxReply;
        TMemo *MemoReply;
        TLabel *Label8;
        TListBox *ListBoxSent;
    void __fastcall Button1Click(TObject *Sender);
        void __fastcall RandomButtonClick(TObject *Sender);
        void __fastcall ButtonSendClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
