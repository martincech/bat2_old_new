//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop


#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <serial32.h>
#include <soubor32.h>
#include <Stringyw.h>

#include "..\..\Intel51\Vym\Bat2\dacsaddr.h"

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;



#define COM_BUFFER_SIZE         127                             // Velikost bufferu seriove linky (definovano p. Wagnerem)
byte            ComBuffer[COM_BUFFER_SIZE];     // Buffer seriove linky
byte            ComBufferCount;                 // Pocet znaku v ComBuffer[]

typedef unsigned char byte;
typedef unsigned short int word;
typedef unsigned long dword;
#define code

#define DLE                     0x10
#define SOH                     0x01
#define ETX                     0x03

typedef union {
   dword DT;
   long  DS;
   struct {
       word Msb;
       word Lsb;
   } Word;
   struct {
       byte X1;
       byte X2;
       byte X3;
       byte X4;
   } ST;
} TUn;

// Union pro konverze wordu :
typedef union {
   word DS;
   struct {
      byte Y1;
      byte Y2;
   } SR;
} TUs;


// Rozdeleni pohlavi
typedef enum {
  GENDER_FEMALE = 0,                            // Pohlavi samice (pokud se pouziva jen 1 pohlavi, jsou vzdy platne parametry pro samice)
  GENDER_MALE,                                  // Pohlavi samec
  _GENDER_COUNT
} TGender;

// Statistika pro jedno pohlavi
typedef struct {
  long  TargetWeight;                           // Cilova hmotnost
  long  Average;                                // Prumerna hmotnost
  word  Count;                                  // Pocet vazeni
  long  WeightNow;
  long  Gain;                                   // Denni prirustek
  byte  Uniformity;                             // Uniformita v %
  short int  Sigma;
  short int  Cv;
} TDacsStats;     // 13 bajtu

// Statistika aktualniho dne zverejnena pro ACS (neni shodna s aktualnim dnem v Bat2)
typedef struct {
  TDacsStats Stats[_GENDER_COUNT];              // Statistika pro jednotlive pohlavi
} TDacsDay;     // 28 bajtu
TDacsDay DacsDay;


//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
    : TForm(Owner)
{
  ComboBoxPort->ItemIndex = 0;
}
//---------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Vypocet CRC
//-----------------------------------------------------------------------------

static byte code TblCrcHi[] = {
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,
0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,
0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,
0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,
0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,
0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,
0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,
0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,
0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,
0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40
} ;

static byte code TblCrcLo[] = {
0x00,0xC0,0xC1,0x01,0xC3,0x03,0x02,0xC2,0xC6,0x06,0x07,0xC7,0x05,0xC5,0xC4,
0x04,0xCC,0x0C,0x0D,0xCD,0x0F,0xCF,0xCE,0x0E,0x0A,0xCA,0xCB,0x0B,0xC9,0x09,
0x08,0xC8,0xD8,0x18,0x19,0xD9,0x1B,0xDB,0xDA,0x1A,0x1E,0xDE,0xDF,0x1F,0xDD,
0x1D,0x1C,0xDC,0x14,0xD4,0xD5,0x15,0xD7,0x17,0x16,0xD6,0xD2,0x12,0x13,0xD3,
0x11,0xD1,0xD0,0x10,0xF0,0x30,0x31,0xF1,0x33,0xF3,0xF2,0x32,0x36,0xF6,0xF7,
0x37,0xF5,0x35,0x34,0xF4,0x3C,0xFC,0xFD,0x3D,0xFF,0x3F,0x3E,0xFE,0xFA,0x3A,
0x3B,0xFB,0x39,0xF9,0xF8,0x38,0x28,0xE8,0xE9,0x29,0xEB,0x2B,0x2A,0xEA,0xEE,
0x2E,0x2F,0xEF,0x2D,0xED,0xEC,0x2C,0xE4,0x24,0x25,0xE5,0x27,0xE7,0xE6,0x26,
0x22,0xE2,0xE3,0x23,0xE1,0x21,0x20,0xE0,0xA0,0x60,0x61,0xA1,0x63,0xA3,0xA2,
0x62,0x66,0xA6,0xA7,0x67,0xA5,0x65,0x64,0xA4,0x6C,0xAC,0xAD,0x6D,0xAF,0x6F,
0x6E,0xAE,0xAA,0x6A,0x6B,0xAB,0x69,0xA9,0xA8,0x68,0x78,0xB8,0xB9,0x79,0xBB,
0x7B,0x7A,0xBA,0xBE,0x7E,0x7F,0xBF,0x7D,0xBD,0xBC,0x7C,0xB4,0x74,0x75,0xB5,
0x77,0xB7,0xB6,0x76,0x72,0xB2,0xB3,0x73,0xB1,0x71,0x70,0xB0,0x50,0x90,0x91,
0x51,0x93,0x53,0x52,0x92,0x96,0x56,0x57,0x97,0x55,0x95,0x94,0x54,0x9C,0x5C,
0x5D,0x9D,0x5F,0x9F,0x9E,0x5E,0x5A,0x9A,0x9B,0x5B,0x99,0x59,0x58,0x98,0x88,
0x48,0x49,0x89,0x4B,0x8B,0x8A,0x4A,0x4E,0x8E,0x8F,0x4F,0x8D,0x4D,0x4C,0x8C,
0x44,0x84,0x85,0x45,0x87,0x47,0x46,0x86,0x82,0x42,0x43,0x83,0x41,0x81,0x80,
0x40
};

static word CalcCrc( word Size)
// Vypocita zabezpeceni z <Size> bytu
{
byte CrcHi = 0;
byte CrcLo = 0;
byte Index;
word i;

   for( i = 1; i < Size; i++){          // Vynecham prvni bajt DLE
      Index = CrcLo ^ ComBuffer[ i];
      CrcLo = CrcHi ^ TblCrcHi[ Index];
      CrcHi = TblCrcLo[ Index];
   }
   return( (word)CrcHi << 8 | CrcLo);
} // CalcCrc

// ---------------------------------------------------------------------------------------------
// Zdvojeni znaku DLE
// ---------------------------------------------------------------------------------------------

static void AddDoubleDLE() {
  // Zdvojeni znaku DLE v bufferu, zaroven adekvatne zvysi ComBufferCount
  byte Index, j;

  // Kontroluju od polozky DNODE do CRC. Polozky SOH a ETX nejsou zdvojene
  for (Index = 2; Index < ComBufferCount - 2; Index++) {
    if (ComBuffer[Index] == DLE) {
      // Znak DLE, musim ho zdvojit
      for (j = ComBufferCount; j > Index; j--) {
        ComBuffer[j] = ComBuffer[j - 1];
      }
      ComBufferCount++;         // Prodlouzim o 1 znak
      Index++;                                      // I index posunu o 1 znak dale, musim preskocit zdvojeny znak DLE
    }
  }
}

//-----------------------------------------------------------------------------
// Prace s bufferem
//-----------------------------------------------------------------------------

void BufferAdd(byte Number) {
  ComBuffer[ComBufferCount++] = Number;
}

void BufferClear(void) {
  ComBufferCount = 0;
}

void BufferVersion() {
  BufferClear();
  BufferAdd(0x10);      // DLE
  BufferAdd(0x01);      // SOH
  BufferAdd(0x41);      // Adresa vahy = 65
  BufferAdd(0xFE);      // Adresa ACS6 = 254
  BufferAdd(0x01);      // 2x delka dat = 1 bajt
  BufferAdd(0x00);
  BufferAdd(0xD0);      // Data
  BufferAdd(0x5C);      // 2x CRC
  BufferAdd(0x6A);
  BufferAdd(0x10);      // DLE
  BufferAdd(0x03);      // ETX

/*
  // Vaha by mela odpovedet:
  0x10          // DLE
  0x01          // SOH
  0xFE          // Adresa prijemce
  0x41          // Adresa vahy = 65
  0x04          // LEN = 4 bajty
  0xD1          // Kod odpovedi
  0x00          // Rezerva
  0x0B          // Minor cislo verze = 11
  0x01          // Major cislo verze = 1
  0xDF          // CRC LSB
  0x1E          // CRC MSB
  0x10          // DLE
  0x03          // ETX
*/
}

void BufferStatistics() {
  BufferClear();
  BufferAdd(0x10);      // DLE
  BufferAdd(0x01);      // SOH
  BufferAdd(0x41);      // Adresa vahy = 65
  BufferAdd(0xFE);      // Adresa ACS6 = 254
  BufferAdd(0x09);      // 2x delka dat = 1 bajt
  BufferAdd(0x00);
  BufferAdd(0xA1);      // Data
  BufferAdd(0x80);
  BufferAdd(0x00);
  BufferAdd(0x00);      // Production day 2 bajty
  BufferAdd(0x00);
  BufferAdd(0x0F);      // Cas 4 bajty
  BufferAdd(0xD8);
  BufferAdd(0x00);
  BufferAdd(0x00);
  BufferAdd(0x06);      // 2x CRC
  BufferAdd(0x12);
  BufferAdd(0x10);      // DLE
  BufferAdd(0x03);      // ETX
}

//---------------------------------------------------------------------------
// Pridani CRC do bufferu
//---------------------------------------------------------------------------

static void AddCrc(void) {
  // Vypocte CRC a prida ho na konec paketu. Paket konci polozkou DATA.
  TUs Crc;

  // Vypoctu CRC zpravy
  Crc.DS = CalcCrc(ComBufferCount);             // Paket konci polozkou DATA, pocitam tedy ze vsech bajtu

  // Pridam CRC do bufferu
  BufferAdd(Crc.SR.Y1);   // LSB
  BufferAdd(Crc.SR.Y2);   // MSB
}

//---------------------------------------------------------------------------
// Sestavi dotaz na statistiku podle zadaneho cisla dne a casu
//---------------------------------------------------------------------------

void BufferStatistics(int ProductionDay, TDateTime Date, TDateTime Time) {
  BufferClear();
  BufferAdd(0x10);      // DLE
  BufferAdd(0x01);      // SOH
  BufferAdd(0x41);      // Adresa vahy = 65
  BufferAdd(0xFE);      // Adresa ACS6 = 254
  BufferAdd(0x0C);      // 2x delka dat = 1 bajt
  BufferAdd(0x00);
  BufferAdd(0xA1);      // Data
  BufferAdd(0x80);
  BufferAdd(0x00);

  // Production day 2 bajty
  BufferAdd(ProductionDay & 0xFF);
  BufferAdd(ProductionDay >> 8);

  // Datum a cas 7 bajtu
  unsigned short year, month, day, hour, min, sec, msec;
  Date.DecodeDate(&year, &month, &day);
  Time.DecodeTime(&hour, &min, &sec, &msec);
  BufferAdd(day);
  BufferAdd(month);
  BufferAdd(year & 0xFF);
  BufferAdd(year >> 8);
  BufferAdd(hour);
  BufferAdd(min);
  BufferAdd(sec);

  // CRC
  AddCrc();

  BufferAdd(0x10);      // DLE
  BufferAdd(0x03);      // ETX

  // Zdvojim znak DLE
  AddDoubleDLE();
}

void BufferBat2Version() {
  // Co posila vaha zpet
  BufferClear();
  BufferAdd(0x10);      // DLE
  BufferAdd(0x01);      // SOH
  BufferAdd(0xFE);      // Adresa ACS6 = 254
  BufferAdd(0x41);      // Adresa vahy = 65
  BufferAdd(0x04);      // 2x delka dat = 4 bajty
  BufferAdd(0x00);      
  BufferAdd(0xD1);
  BufferAdd(0x00);
  BufferAdd(0x0B);
  BufferAdd(0x01);
  BufferAdd(0x2C);      // 2x CRC
  BufferAdd(0x14);
  BufferAdd(0x10);      // DLE
  BufferAdd(0x03);      // ETX
}

void __fastcall TForm1::Button1Click(TObject *Sender)
{
  HANDLE IdPort;

  // Otevru port
  try {
    if (!OtevriPort(ComboBoxPort->ItemIndex + 1, &IdPort,9600,8,0,1)) throw(1);
    if (!NastaveniBuferu(IdPort,1000,1000)) {
      CloseHandle(IdPort);
      throw(1);
    }
    if (!NastaveniTimeOutu(IdPort,1000)) { // Timeout
      CloseHandle(IdPort);
      throw(1);
    }
  }
  catch (...) {
    ShowMessage("Chyba pri otevirani portu");
    return;
  }

  // Vyvorim paket
//  BufferVersion();
//  BufferBat2Version();
  BufferStatistics();
  
  try {
    Screen->Cursor = crHourGlass;

    // Poslu paket
    ZapisPort(IdPort, ComBuffer, ComBufferCount);

    // Nactu odpoved
    strset(ComBuffer, 0);
    int i = NactiPort(IdPort, ComBuffer, 200);

    // Zobrazim
    Label2->Caption = (char *)ComBuffer;
  } catch (...) {}

  Screen->Cursor = crDefault;

  // Zavru port
  CloseHandle(IdPort);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::RandomButtonClick(TObject *Sender)
{
  HANDLE IdPort;

  // Otevru port
  try {
    if (!OtevriPort(ComboBoxPort->ItemIndex + 1, &IdPort,9600,8,2,1)) throw(1);
    if (!NastaveniBuferu(IdPort,1000,1000)) {
      CloseHandle(IdPort);
      throw(1);
    }
    if (!NastaveniTimeOutu(IdPort,300)) { // Timeout
      CloseHandle(IdPort);
      throw(1);
    }
  }
  catch (...) {
    ShowMessage("Chyba pri otevirani portu");
    return;
  }


  randomize();
  for (int i = 0; i < 100; i++) {
    ComBuffer[0] = rand() % 100;
    ZapisPort(IdPort, ComBuffer, 1);
  }


  // Nactu odpoved
  strset(ComBuffer, 0);
  int i = NactiPort(IdPort, ComBuffer, 200);

  // Zavru port
  CloseHandle(IdPort);

  // Zobrazim
  Label2->Caption = (char *)ComBuffer;

  i = 0;
}
//---------------------------------------------------------------------------

void DecodeGender(byte *Buffer, TGender Gender) {
  byte *p;
  p = (byte*) &DacsDay.Stats[Gender];
  for (int i = 0; i < sizeof(TDacsStats); i++) {
    *p = *Buffer;
    p++;
    Buffer++;
  }
}

void DecodeBuffer(byte *Buffer) {
  DecodeGender(&Buffer[9], GENDER_FEMALE);
  DecodeGender(&Buffer[32], GENDER_MALE);
}

void ShowGender(TMemo *Memo, TGender Gender) {
  Memo->Lines->Add("Target     = " + String(DacsDay.Stats[Gender].TargetWeight));
  Memo->Lines->Add("Average    = " + String(DacsDay.Stats[Gender].Average));
  Memo->Lines->Add("Count      = " + String(DacsDay.Stats[Gender].Count));
  Memo->Lines->Add("WeightNow  = " + String(DacsDay.Stats[Gender].WeightNow));
  Memo->Lines->Add("Gain       = " + String(DacsDay.Stats[Gender].Gain));
  Memo->Lines->Add("Uniformity = " + String(DacsDay.Stats[Gender].Uniformity));
  Memo->Lines->Add("Sigma      = " + String(DacsDay.Stats[Gender].Sigma));
  Memo->Lines->Add("Cv         = " + String(DacsDay.Stats[Gender].Cv));
}

void __fastcall TForm1::ButtonSendClick(TObject *Sender)
{
  HANDLE IdPort;

  // Otevru port
  try {
    if (!OtevriPort(ComboBoxPort->ItemIndex + 1, &IdPort,9600,8,0,1)) throw(1);
    if (!NastaveniBuferu(IdPort,1000,1000)) {
      CloseHandle(IdPort);
      throw(1);
    }
    if (!NastaveniTimeOutu(IdPort,1000)) { // Timeout
      CloseHandle(IdPort);
      throw(1);
    }
  }
  catch (...) {
    ShowMessage("Chyba pri otevirani portu");
    return;
  }

  // Vyvorim paket
  BufferStatistics(EditProductionDay->Text.ToInt(), DateTimePickerDate->DateTime, DateTimePickerTime->DateTime);

  try {
    Screen->Cursor = crHourGlass;

    // Poslu paket
    ZapisPort(IdPort, ComBuffer, ComBufferCount);

    // Zobrazim zaslanou zpravu
    String Str;
    char s[5];
    ListBoxSent->Items->Clear();
    for (int j = 0; j < ComBufferCount; j++) {
      itoa(ComBuffer[j], s, 16);
      if (ComBuffer[j] < 0x10) {
        Str = "0" + String(s);
      } else {
        Str = String(s);
      }
      ListBoxSent->Items->Add(FormatFloat("00", j) + ":       " + Str.UpperCase() + "   (" + String(ComBuffer[j]) + ")");
    }

    // Nactu odpoved
    strset(ComBuffer, 0);
    int i = NactiPort(IdPort, ComBuffer, 200);

    // Zobrazim
    ListBoxReply->Items->Clear();
    for (int j = 0; j < i; j++) {
      itoa(ComBuffer[j], s, 16);
      if (ComBuffer[j] < 0x10) {
        Str = "0" + String(s);
      } else {
        Str = String(s);
      }
      ListBoxReply->Items->Add(FormatFloat("00", j) + ":       " + Str.UpperCase() + "   (" + String(ComBuffer[j]) + ")");
    }

    // Dekoduju a zobrazim
    MemoReply->Clear();
    if (i > 0) {
      DecodeBuffer(ComBuffer);
      ShowGender(MemoReply, GENDER_FEMALE);
      MemoReply->Lines->Add("---");
      ShowGender(MemoReply, GENDER_MALE);
    }


  } catch (...) {}

  Screen->Cursor = crDefault;

  // Zavru port
  CloseHandle(IdPort);
}
//---------------------------------------------------------------------------

