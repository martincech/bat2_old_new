#pragma once

/*! \file crc16.h
 *	\brief Hlavickovy soubor
 *  \author SofCon (c) 2006, Ondrej Netik
 *  \version 1.0
 *  \date    2006-09-13
 */

#include<windows.h>

WORD addCRC16( WORD CRC, BYTE b );
WORD calcCRC16( WORD data );
WORD blockCRC16(char *start, DWORD len, WORD Res);
unsigned char blockCrc8(unsigned char *buff, unsigned int len);
unsigned char addCrc8(unsigned char CRC, unsigned char dato);